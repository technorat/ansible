# Плейбуки и роли для развертывания сервисов

## Плейбуки

1. **change_pass.yml** - меняет пароль пользователю "user". ВНИМАНИЕ! Пароль надо задать в самом плейбуке. Без роли.
  
1. **delete_grub_password.yml** - Удаляет пароль в файле /etc/grub.d/07_password чтобы при загрузка сервера происходила без участия пользователя. Без роли.

1. **docker.yml** - пытается установить Docker. Роль - "docker"
  
1. **rabbit-single-node.yml** - устанавливает Rabbit-mq в режиме single-node. Роль - "rabbitmq-single-node"

## Роли

1. **delete_grub_password** 

1. **docker** - кривоватая, работает не везде, требуется допиливание.
 
1. **rabbitmq-single-node**
