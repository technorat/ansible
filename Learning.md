# Разные интересные заметки по Ansible
* Вывести собранные с машины факты
```
- name: Show facts available on the system
  debug:
    var: ansible_facts
```

* Вывести семейство OS
```
- name: Show OS family
  debug:
    var: ansible_facts['os_family']
```
или так:
```
- name: Show OS family
  debug:
    msg: "OS family: {{ ansible_os_family }}"
```

* Версия ОС
```
- name: AnsibleUnsafeText
  debug: 
    var: ansible_facts['distribution']

- name: Show ansible_facts
  debug:
    var: ansible_distribution_release.split('_').0
```

* Выполнять роль если у нас Астра Линукс
```
  roles:
    - { role: astralinux17, when: ansible_os_family == 'Astra Linux' }
```

