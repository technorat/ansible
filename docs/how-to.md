# Рецепты/сценарии 

### Как сделать файл с именем, в котором содержится дата и время
```
# Архивируем рабочую папку и кладем в папку с резервными копиями
- name: Compress work directory and move to 
  archive:
    path: /<исходная папка>/
    dest: "/<конечная папка>/backup-{{ '%Y%m%d_%H.%M.%S' | strftime }}.bz2"
    format: bz2

``` 

### Как сделать очистку папки с бэкапами, оставив только 5 последних копий?
```
# 3.1. Удаление старых резервных копий | поиск файлов в папке
- name: Get all files
  find:
    paths: "/backups"
    file_type: file
    recurse: no
  register: install_artifacts

# 3.2. Удаление старых резервных копий | делаем сортировку, отделяем старые
- name: Determine old files
  set_fact:
    old_dirs: "{{ (install_artifacts.files|sort(attribute='mtime', reverse=True))[5:] }}"

# 3.3. Удаление старых резервных копий | удаляем самые старые файлы
- name: Remove old directories
  file:
    path: "{{ item.path }}"
    state: absent
  with_items: "{{ old_dirs }}"
  when: install_artifacts.matched > 5
```