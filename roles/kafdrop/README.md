#Role Name
=========
Install PostgreSQL server into docker compose

##Requirements
------------
Change target server in ../../playbooks/postgresql.yml before run!

##Dependencies
------------
This rile used image "postgres:latest" (https://hub.docker.com/_/postgres)

##How to use
------------
ansible-playbook <ansible-directory>/playbooks/postgresql.yml

##Author Information
------------------
Autor: Vyshegorodskiy Sergey Petrovich (darksoft2021)
e-mail: svyshegorodskiy@fil-it.ru
